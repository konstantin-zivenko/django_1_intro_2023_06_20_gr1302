from django.shortcuts import render
from django.http import HttpRequest, HttpResponse


# Create your views here.
def hello_world(request: HttpRequest) -> HttpResponse:
    return render(request, "hello_world.html", {})


def hello_world_simply(request: HttpRequest) -> HttpResponse:
    return HttpResponse("hello, simply world!")
